/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tampilan;

import hotelreservation.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author titan
 */
public class FormPengunjung extends javax.swing.JFrame {

    Connection koneksi;
    String id_aktif = null;

    /**
     * Creates new form FormTamu
     */
    public FormPengunjung() {
        initComponents();

        tblData.getSelectionModel().addListSelectionListener((ListSelectionEvent arg0) -> {
            int selected_row = tblData.getSelectedRow();
            TableModel dtm = tblData.getModel();

            if (selected_row > -1) {
                Object id = dtm.getValueAt(selected_row, 0);
                getRow(String.valueOf(id));
                btnHapus.setEnabled(true);
            }
        });

        try {
            koneksi = DBConnect.getConnection();
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(FormHarga.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void getRow(String id) {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT * FROM tbl_pengunjung";
            query += String.format(" WHERE id = '%s'", id);
            ResultSet rs = stat.executeQuery(query);

            if (rs.next()) {
                LocalDate dob = rs.getDate("tgl_lahir").toLocalDate();
                Date tgl = Date.from(dob.atStartOfDay(ZoneId.systemDefault()).toInstant());
                btnGroupJK.clearSelection();
                id_aktif = rs.getString("id");
                txtID.setText(rs.getString("id"));
                txtNama.setText(rs.getString("nama_lengkap"));
                if ("Perempuan".equals(rs.getString("jenis_kelamin"))) {
                    radioPerempuan.setSelected(true);
                } else {
                    radioLaki.setSelected(true);
                }
                txtDOB.setValue(tgl);
                chkBlock.setSelected(rs.getInt("is_blocked") == 1);
            } else {
                JOptionPane.showMessageDialog(null, "Tamu tidak ditemukan", "Tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void refreshTable() {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT * FROM tbl_pengunjung";
            String keyword = txtCari.getText();
            if (!keyword.isEmpty()) {
                query += String.format(" WHERE nama_lengkap LIKE '%%%s%%'", keyword);
            }

            ResultSet rs = stat.executeQuery(query);
            DefaultTableModel dtm = (DefaultTableModel) tblData.getModel();
            dtm.setRowCount(0);
            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getDate(4),
                    rs.getInt(5) == 1,
                });
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void clear() {
        id_aktif = null;
        txtID.setText("");
        txtNama.setText("");
        btnGroupJK.setSelected(radioLaki.getModel(), true);
        txtDOB.setText("");
        chkBlock.setSelected(false);
        txtNama.requestFocus();
        tblData.clearSelection();
        btnHapus.setEnabled(false);
    }

    protected void simpan() {
        String id = txtID.getText();
        String nama = txtNama.getText();
        String jenis_kelamin = radioPerempuan.isSelected() ? "Perempuan" : "Laki-Laki";
        Date tgl_lahir = (Date) txtDOB.getValue();
        LocalDate tgl_lahir_sql = tgl_lahir.toInstant().atOffset(ZoneOffset.ofHours(7)).toLocalDate();
        int blok = chkBlock.isSelected() ? 1 : 0;
        
        try {
            String query = "INSERT INTO tbl_pengunjung (id,nama_lengkap,jenis_kelamin,tgl_lahir,is_blocked) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            stat.setString(1, id);
            stat.setString(2, nama);
            stat.setString(3, jenis_kelamin);
            stat.setDate(4, java.sql.Date.valueOf(tgl_lahir_sql));
            stat.setInt(5, blok);
            int hasil = stat.executeUpdate();

            if (hasil > 0) {
                id_aktif = id;
                btnHapus.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Berhasil menginsert data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void ubah() {
        String id = id_aktif;
        String nama = txtNama.getText();
        String jenis_kelamin = radioPerempuan.isSelected() ? "Perempuan" : "Laki-Laki";
        Date tgl_lahir = (Date) txtDOB.getValue();
        LocalDate tgl_lahir_sql = tgl_lahir.toInstant().atOffset(ZoneOffset.ofHours(7)).toLocalDate();
        int blok = chkBlock.isSelected() ? 1 : 0;
        
        try {
            String query = "UPDATE tbl_pengunjung SET nama_lengkap = ?, jenis_kelamin = ?, tgl_lahir = ?, is_blocked = ? WHERE id = ?";
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            stat.setString(1, nama);
            stat.setString(2, jenis_kelamin);
            stat.setDate(3, java.sql.Date.valueOf(tgl_lahir_sql));
            stat.setInt(4, blok);
            stat.setString(5, id);
            int affect = stat.executeUpdate();

            if (affect > 0) {
                JOptionPane.showMessageDialog(null, "Berhasil mengubah data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void hapus() {
        String id = txtID.getText();

        try {
            String query = String.format("DELETE FROM tbl_pengunjung WHERE id = '%s'", id);
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            int affect = stat.executeUpdate();

            if (affect > 0) {
                JOptionPane.showMessageDialog(null, "Berhasil menghapus data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
                this.clear();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupJK = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        txtNama = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        radioLaki = new javax.swing.JRadioButton();
        radioPerempuan = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        txtDOB = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        btnSimpan = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnTutup = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        chkBlock = new javax.swing.JCheckBox();
        btnCari = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtCari = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Daftar Pengunjung");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setText("Daftar Pengunjung");
        jLabel1.setToolTipText("");

        jLabel2.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel2.setText("No KTP/ID");

        txtID.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        txtNama.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel3.setText("Nama Lengkap");

        btnGroupJK.add(radioLaki);
        radioLaki.setText("Laki-Laki");

        btnGroupJK.add(radioPerempuan);
        radioPerempuan.setText("Perempuan");

        jLabel4.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel4.setText("Jenis Kelamin");

        txtDOB.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));

        jLabel5.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel5.setText("Tanggal Lahir");

        btnSimpan.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnHapus.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnHapus.setText("Hapus");
        btnHapus.setEnabled(false);
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnTutup.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnTutup.setText("Tutup");
        btnTutup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTutupActionPerformed(evt);
            }
        });

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No ID", "Nama Lengkap", "JK", "Tanggal Lahir", "Blokir"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblData);

        chkBlock.setText("Block");

        btnCari.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCari.setText("Cari");
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel6.setText("Kata Kunci Pencarian");

        txtCari.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(radioLaki)
                                        .addGap(42, 42, 42)
                                        .addComponent(radioPerempuan))
                                    .addComponent(txtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(chkBlock)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnTutup, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 109, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCari)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCari)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioLaki)
                    .addComponent(radioPerempuan)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkBlock)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSimpan)
                    .addComponent(btnHapus)
                    .addComponent(btnClear)
                    .addComponent(btnTutup))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(btnCari))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTutupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTutupActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_btnTutupActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        this.clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        if (id_aktif == null) {
            this.simpan();
        } else {
            this.ubah();
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        this.hapus();
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        this.refreshTable();
    }//GEN-LAST:event_btnCariActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.refreshTable();
    }//GEN-LAST:event_formWindowOpened

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnClear;
    private javax.swing.ButtonGroup btnGroupJK;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btnTutup;
    private javax.swing.JCheckBox chkBlock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioLaki;
    private javax.swing.JRadioButton radioPerempuan;
    private javax.swing.JTable tblData;
    private javax.swing.JTextField txtCari;
    private javax.swing.JFormattedTextField txtDOB;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtNama;
    // End of variables declaration//GEN-END:variables
}
