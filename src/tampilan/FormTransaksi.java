/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tampilan;

import hotelreservation.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author titan
 */
public class FormTransaksi extends javax.swing.JFrame {

    Connection koneksi;
    String id_tamu = null;
    long id_kamar = 0;
    long id_aktif = 0;

    /**
     * Creates new form FormTransaksi
     */
    public FormTransaksi() {
        initComponents();

        tblData.getSelectionModel().addListSelectionListener((ListSelectionEvent arg0) -> {
            int selected_row = tblData.getSelectedRow();
            TableModel dtm = tblData.getModel();

            if (selected_row > -1) {
                Object id = dtm.getValueAt(selected_row, 0);
                bacaBaris(String.valueOf(id));
            }
        });

        try {
            koneksi = DBConnect.getConnection();
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(FormHarga.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private java.sql.Date toSqlDate(java.util.Date tgl) {
        if (tgl == null) {
            return null;
        }

        LocalDate tgl_lokal = tgl.toInstant().atOffset(ZoneOffset.ofHours(7)).toLocalDate();
        return java.sql.Date.valueOf(tgl_lokal);
    }

    private java.util.Date toLocalDate(java.sql.Date tgl) {
        if (tgl == null) {
            return null;
        }

        LocalDate tgl_lokal = tgl.toLocalDate();
        return Date.from(tgl_lokal.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private void bacaBaris(String id) {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT tbl_transaksi.*, tbl_kamar.nomor_kamar, tbl_harga.harga,";
            query += " tbl_harga.nama_paket, tbl_pengunjung.nama_lengkap FROM tbl_transaksi";
            query += " JOIN tbl_pengunjung ON tbl_pengunjung.id = tbl_transaksi.id_pengunjung";
            query += " JOIN tbl_kamar ON tbl_kamar.id = tbl_transaksi.id_kamar";
            query += " JOIN tbl_harga ON tbl_harga.id = tbl_kamar.id_harga";
            query += String.format(" WHERE tbl_transaksi.id = '%s'", id);
            ResultSet rs = stat.executeQuery(query);

            if (rs.next()) {
                this.id_aktif = rs.getLong("id");
                this.id_kamar = rs.getLong("id_kamar");
                this.id_tamu = rs.getString("id_pengunjung");
                this.txtKamar.setText(rs.getString("nomor_kamar"));
                this.txtPengunjung.setText(rs.getString("nama_lengkap"));
                this.txtCheckin.setValue(toLocalDate(rs.getDate("tgl_checkin")));
                this.txtCheckout.setValue(toLocalDate(rs.getDate("tgl_checkout")));
                this.txtTagihan.setValue(rs.getInt("jml_bayar"));
                txtCheckin.setEditable(false);
                txtCheckout.setEditable(true);
                btnCariKamar.setEnabled(false);
                btnCariPengunjung.setEnabled(false);

                if ("selesai".equals(rs.getString("status"))) {
                    txtCheckout.setEditable(false);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Transaksi tidak ditemukan", "Tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    private void simpanData() {
        long id_kamar_ = this.id_kamar;
        String id_tamu_ = this.id_tamu;
        Date checkin = (Date) txtCheckin.getValue();
        String query = "INSERT INTO tbl_transaksi (id_kamar,id_pengunjung,tgl_checkin,status) VALUES (?,?,?,?)";

        try {
            PreparedStatement stat = this.koneksi.prepareStatement(query, new String[]{"id"});
            stat.setLong(1, id_kamar_);
            stat.setString(2, id_tamu_);
            stat.setDate(3, toSqlDate(checkin));
            stat.setString(4, "aktif");
            stat.executeUpdate();

            ResultSet rs = stat.getGeneratedKeys();

            if (rs.next()) {
                updateStatusKamar(id_kamar, "digunakan");
                long id = rs.getLong(1);
                this.id_aktif = id;
                JOptionPane.showMessageDialog(null, "Berhasil menyimpan transaksi", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshData();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    private void updateData() {
        long id = id_aktif;
        Date checkin = (Date) txtCheckin.getValue();
        Date checkout = (Date) txtCheckout.getValue();

        try {
            String query = "UPDATE tbl_transaksi SET status = ?, tgl_checkout = ?, jml_bayar = ? WHERE id = ?";
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            stat.setString(1, "selesai");
            stat.setDate(2, toSqlDate(checkout));
            stat.setDouble(3, calculatePrice(id_kamar, checkin, checkout));
            stat.setLong(4, id);
            int affect = stat.executeUpdate();

            if (affect > 0) {
                updateStatusKamar(id_kamar, "kosong");
                JOptionPane.showMessageDialog(null, "Berhasil mengubah data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshData();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    private int updateStatusKamar(long id_kamar, String status) {
        try {
            String query = "update tbl_kamar set status = ? where id = ?";
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            stat.setString(1, status);
            stat.setLong(2, id_kamar);

            return stat.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FormTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    private double calculatePrice(long id_kamar, Date start, Date end) {
        String query = "select tbl_harga.harga from tbl_kamar";
        query += " join tbl_harga on tbl_harga.id = tbl_kamar.id_harga";
        query += " where tbl_kamar.id = ?";

        try {
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            stat.setLong(1, id_kamar);
            ResultSet rs = stat.executeQuery();

            if (rs.next()) {
                long diff = Math.abs(start.getTime() - end.getTime());
                double harga = rs.getDouble("harga");
                double dayCount = Math.ceil((float) diff / (24 * 60 * 60 * 1000));
                
                return harga * dayCount;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FormTransaksi.class.getName()).log(Level.SEVERE, null, ex);
        }

        return 0;
    }

    private void clear() {
        this.id_aktif = 0;
        this.id_kamar = 0;
        this.id_tamu = "";
        this.txtCheckin.setText("");
        this.txtCheckout.setText("");
        this.txtPengunjung.setText("");
        this.txtKamar.setText("");
        txtTagihan.setValue(null);
        txtCheckin.setValue(null);
        txtCheckout.setValue(null);
        txtCheckin.setEditable(true);
        txtCheckout.setEditable(false);
        btnCariKamar.setEnabled(true);
        btnCariPengunjung.setEnabled(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCariPengunjung = new javax.swing.JButton();
        txtPengunjung = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtKamar = new javax.swing.JTextField();
        btnCariKamar = new javax.swing.JButton();
        txtCheckin = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtCheckout = new javax.swing.JFormattedTextField();
        btnSimpan = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        txtCari = new javax.swing.JTextField();
        btnCari = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtTagihan = new javax.swing.JFormattedTextField();
        btnTutup = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Transaksi Check In/Check Out");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnCariPengunjung.setText("Cari Data");
        btnCariPengunjung.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariPengunjungActionPerformed(evt);
            }
        });

        txtPengunjung.setEditable(false);
        txtPengunjung.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Pengunjung");

        jLabel2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel2.setText("Transaksi CheckIn/Checkout");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Kamar");

        txtKamar.setEditable(false);
        txtKamar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnCariKamar.setText("Cari Data");
        btnCariKamar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariKamarActionPerformed(evt);
            }
        });

        txtCheckin.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        txtCheckin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("Tgl Check In");

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setText("Tgl Check Out");

        txtCheckout.setEditable(false);
        txtCheckout.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        txtCheckout.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Pengunjung", "Kamar", "Status", "Tgl Check Out", "Tgl Check In", "Tagihan"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblData);

        txtCari.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnCari.setText("Cari");
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel6.setText("Tagihan");

        txtTagihan.setEditable(false);
        txtTagihan.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("Rp#,##0"))));
        txtTagihan.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnTutup.setText("Tutup");
        btnTutup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTutupActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtCari)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCari))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3))
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtPengunjung, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnCariPengunjung))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtKamar, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnCariKamar))))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtCheckin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtCheckout, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtTagihan, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnSimpan)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnClear)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnTutup))))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPengunjung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCariPengunjung))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtKamar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCariKamar))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCheckin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCheckout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTagihan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSimpan)
                    .addComponent(btnTutup)
                    .addComponent(btnClear))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCari))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCariPengunjungActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariPengunjungActionPerformed
        DialogCariTamu dlgtamu = new DialogCariTamu(this, true);
        dlgtamu.setVisible(true);

        Object terpilih = dlgtamu.getIdTerpilih();

        if (terpilih == null) {
            //
        } else {
            Object[] terpilih_values = (Object[]) terpilih;

            id_tamu = String.valueOf(terpilih_values[0]);
            txtPengunjung.setText(String.valueOf(terpilih_values[1]));
        }
    }//GEN-LAST:event_btnCariPengunjungActionPerformed

    private void btnCariKamarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariKamarActionPerformed
        DialogCariKamar dlgkamar = new DialogCariKamar(this, true);
        dlgkamar.setVisible(true);

        Object terpilih = dlgkamar.getIdTerpilih();

        if (terpilih == null) {
            //
        } else {
            Object[] terpilih_values = (Object[]) terpilih;

            id_kamar = (long) terpilih_values[0];
            txtKamar.setText(String.valueOf(terpilih_values[1]));
        }
    }//GEN-LAST:event_btnCariKamarActionPerformed

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        if (id_aktif == 0) {
            this.simpanData();
        } else {
            this.updateData();
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        this.refreshData();
    }//GEN-LAST:event_btnCariActionPerformed

    private void btnTutupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTutupActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_btnTutupActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.refreshData();
    }//GEN-LAST:event_formWindowOpened

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        this.clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void refreshData() {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT tbl_transaksi.*, tbl_kamar.nomor_kamar, tbl_harga.harga,";
            query += " tbl_harga.nama_paket, tbl_pengunjung.nama_lengkap FROM tbl_transaksi";
            query += " JOIN tbl_pengunjung ON tbl_pengunjung.id = tbl_transaksi.id_pengunjung";
            query += " JOIN tbl_kamar ON tbl_kamar.id = tbl_transaksi.id_kamar";
            query += " JOIN tbl_harga ON tbl_harga.id = tbl_kamar.id_harga";
            String keyword = txtCari.getText();
            if (!keyword.isEmpty()) {
                query += String.format(" WHERE tbl_pengunjung.nama_lengkap LIKE '%%%s%%'", keyword);
            }

            query += " ORDER BY tbl_transaksi.tgl_checkin DESC";

            ResultSet rs = stat.executeQuery(query);
            DefaultTableModel dtm = (DefaultTableModel) tblData.getModel();
            dtm.setRowCount(0);
            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getInt("id"),
                    rs.getString("nama_lengkap"),
                    String.format("%s (%s)", rs.getString("nomor_kamar"), rs.getString("nama_paket")),
                    rs.getString("status"),
                    rs.getDate("tgl_checkin"),
                    rs.getDate("tgl_checkout"),
                    rs.getInt("jml_bayar"),});
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormTransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormTransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormTransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormTransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FormTransaksi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnCariKamar;
    private javax.swing.JButton btnCariPengunjung;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btnTutup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblData;
    private javax.swing.JTextField txtCari;
    private javax.swing.JFormattedTextField txtCheckin;
    private javax.swing.JFormattedTextField txtCheckout;
    private javax.swing.JTextField txtKamar;
    private javax.swing.JTextField txtPengunjung;
    private javax.swing.JFormattedTextField txtTagihan;
    // End of variables declaration//GEN-END:variables
}
