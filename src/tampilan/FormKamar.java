/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tampilan;

import hotelreservation.DBConnect;
import hotelreservation.Harga;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author titan
 */
public class FormKamar extends javax.swing.JFrame {

    java.util.ArrayList<Harga> daftar_harga;
    Connection koneksi;
    int paketId = 0;
    ListSelectionListener listener;
    DefaultTableModel dtm = new DefaultTableModel(
            new Object[][]{},
            new String[]{
                "No ID", "Nomor Kamar", "ID Harga", "Paket Harga", "Aktif", "Status"
            }
    ) {
        Class[] types = new Class[]{
            java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Boolean.class, java.lang.String.class
        };
        boolean[] canEdit = new boolean[]{
            false, false, false, false, false, false
        };

        @Override
        public Class getColumnClass(int columnIndex) {
            return types[columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit[columnIndex];
        }
    };

    /**
     * Creates new form FormKamar
     */
    public FormKamar() {
        this.daftar_harga = new ArrayList<>();
        this.listener = (ListSelectionEvent arg0) -> {
            int selected_row = tblData.getSelectedRow();

            if (selected_row > -1) {
                Object id = dtm.getValueAt(selected_row, 0);
                getRow(String.valueOf(id));
                btnHapus.setEnabled(true);
            }
        };

        initComponents();

        tblData.getSelectionModel().addListSelectionListener(listener);
        tblData.setModel(dtm);

        try {
            koneksi = DBConnect.getConnection();
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(FormHarga.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void getRow(String id) {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT tbl_kamar.id, nomor_kamar, id_harga, tbl_harga.nama_paket, tbl_harga.harga, kamar_aktif, status";
            query += " FROM tbl_kamar";
            query += " JOIN tbl_harga ON tbl_kamar.id_harga = tbl_harga.id";
            query += String.format(" WHERE tbl_kamar.id = '%s'", id);
            ResultSet rs = stat.executeQuery(query);

            if (rs.next()) {
                String harga = String.format("%s (Rp%d)", rs.getString(4), rs.getInt(5));
                txtID.setText(rs.getString(1));
                txtNomorKamar.setText(rs.getString(2));
                txtPaket.setText(harga);
                txtStatus.setText(rs.getString(7));
                chkAktif.setSelected(rs.getBoolean(6));
                this.paketId = rs.getInt(3);
            } else {
                JOptionPane.showMessageDialog(null, "Karyawan tidak ditemukan", "Tidak ditemukan", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void refreshTable() {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT tbl_kamar.id, nomor_kamar, id_harga, tbl_harga.nama_paket, tbl_harga.harga, kamar_aktif, status FROM tbl_kamar";
            query += " JOIN tbl_harga ON tbl_kamar.id_harga = tbl_harga.id";
            String keyword = txtCari.getText();

            if (!keyword.isEmpty()) {
                query += String.format(" WHERE nomor_kamar LIKE '%%%s%%'", keyword);
            }

            ResultSet rs = stat.executeQuery(query);
            dtm.setRowCount(0);

            while (rs.next()) {
                String harga = String.format("%s (Rp%d)", rs.getString(4), rs.getInt(5));
                dtm.addRow(new Object[]{
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getInt(3),
                    harga,
                    rs.getInt(6) == 1,
                    rs.getString(7)
                });
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void refreshHarga() {
        try {
            Statement stat = this.koneksi.createStatement();
            String query = "SELECT id, nama_paket, harga FROM tbl_harga";
            ResultSet rs = stat.executeQuery(query);
            this.daftar_harga.clear();
            while (rs.next()) {
                int Id = rs.getInt(1);
                int Harga = rs.getInt(3);
                String NamaPaket = rs.getString(2);

                this.daftar_harga.add(new Harga(Id, NamaPaket, Harga));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void clear() {
        txtID.setText("");
        txtNomorKamar.setText("");
        txtPaket.setText("");
        txtStatus.setText("");
        chkAktif.setSelected(false);
        txtNomorKamar.requestFocus();
        tblData.clearSelection();
        btnHapus.setEnabled(false);
        this.paketId = 0;
    }

    protected void simpan() {
        String nomor_kamar = txtNomorKamar.getText();
        int id_harga = this.paketId;
        int aktif = chkAktif.isSelected() ? 1 : 0;
        String status = "kosong";

        if (id_harga == 0) {
            JOptionPane.showMessageDialog(null, "Harap pilih harga", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            String query = String.format("INSERT INTO tbl_kamar (nomor_kamar, id_harga, kamar_aktif, status) VALUES ('%s', '%s', '%s', '%s')", nomor_kamar, id_harga, aktif, status);
            PreparedStatement stat = this.koneksi.prepareStatement(query, new String[]{"id"});

            stat.executeUpdate();

            ResultSet rs = stat.getGeneratedKeys();

            if (rs.next()) {
                long id = rs.getLong(1);
                txtID.setText(String.valueOf(id));
                txtStatus.setText(status);
                btnHapus.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Berhasil menginsert data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void ubah() {
        String id = txtID.getText();
        String nomor_kamar = txtNomorKamar.getText();
        int id_harga = this.paketId;
        int aktif = chkAktif.isSelected() ? 1 : 0;

        if (id_harga == 0) {
            JOptionPane.showMessageDialog(null, "Harap pilih harga", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            String query = String.format("UPDATE tbl_kamar SET nomor_kamar = '%s', id_harga = '%s', kamar_aktif = '%s' WHERE id='%s'", nomor_kamar, id_harga, aktif, id);
            PreparedStatement stat = this.koneksi.prepareStatement(query, new String[]{"id"});
            int affect = stat.executeUpdate();

            if (affect > 0) {
                JOptionPane.showMessageDialog(null, "Berhasil mengubah data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    protected void hapus() {
        String id = txtID.getText();

        try {
            String query = String.format("DELETE FROM tbl_kamar WHERE id = '%s'", id);
            PreparedStatement stat = this.koneksi.prepareStatement(query);
            int affect = stat.executeUpdate();

            if (affect > 0) {
                JOptionPane.showMessageDialog(null, "Berhasil menghapus data", "Sukses", JOptionPane.INFORMATION_MESSAGE);
                this.refreshTable();
                this.clear();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getGlobal().log(Level.WARNING, "Something wrong", ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNomorKamar = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPaket = new javax.swing.JTextField();
        btnCariPaket = new javax.swing.JButton();
        chkAktif = new javax.swing.JCheckBox();
        txtStatus = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnSimpan = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnTutup = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        txtCari = new javax.swing.JTextField();
        btnCari = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Daftar Kamar");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setText("Daftar Kamar");
        jLabel1.setToolTipText("");

        jLabel2.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel2.setText("No. ID Kamar");

        txtID.setEditable(false);
        txtID.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel3.setText("Nomor Kamar");

        txtNomorKamar.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel4.setText("Paket Harga");

        txtPaket.setEditable(false);
        txtPaket.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        btnCariPaket.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnCariPaket.setText("Cari");
        btnCariPaket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariPaketActionPerformed(evt);
            }
        });

        chkAktif.setText("Kamar Aktif");

        txtStatus.setEditable(false);
        txtStatus.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        jLabel5.setText("Status Kamar");

        btnSimpan.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnSimpan.setText("Simpan");
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnHapus.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnHapus.setText("Hapus");
        btnHapus.setEnabled(false);
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnTutup.setFont(new java.awt.Font("Fira Sans", 0, 14)); // NOI18N
        btnTutup.setText("Tutup");
        btnTutup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTutupActionPerformed(evt);
            }
        });

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No ID", "Nomor Kamar", "ID Harga", "Paket Harga", "Aktif", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Boolean.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblData);

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel6.setText("Kata Kunci Pencarian");

        txtCari.setFont(new java.awt.Font("Dialog", 0, 13)); // NOI18N

        btnCari.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCari.setText("Cari");
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCari)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCari))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(chkAktif)
                                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtNomorKamar, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtPaket, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnCariPaket))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnTutup, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNomorKamar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPaket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCariPaket))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chkAktif)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSimpan)
                    .addComponent(btnHapus)
                    .addComponent(btnClear)
                    .addComponent(btnTutup))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(btnCari))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        String id = txtID.getText();

        if (id.isBlank()) {
            this.simpan();
        } else {
            this.ubah();
        }
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        this.hapus();
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        this.clear();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnTutupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTutupActionPerformed
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_btnTutupActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        this.refreshTable();
    }//GEN-LAST:event_btnCariActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.refreshHarga();
        this.refreshTable();
    }//GEN-LAST:event_formWindowOpened

    private void btnCariPaketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariPaketActionPerformed
        DialogCariHarga dlgharga = new DialogCariHarga(this, true, this.daftar_harga);
        dlgharga.setVisible(true);

        Harga terpilih = dlgharga.getHarga();

        if (terpilih == null) {
            //
        } else {
            String harga = String.format("%s (Rp%d)", terpilih.NamaPaket, terpilih.Harga);
            this.paketId = terpilih.Id;
            this.txtPaket.setText(harga);
        }
    }//GEN-LAST:event_btnCariPaketActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnCariPaket;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btnTutup;
    private javax.swing.JCheckBox chkAktif;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblData;
    private javax.swing.JTextField txtCari;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtNomorKamar;
    private javax.swing.JTextField txtPaket;
    private javax.swing.JTextField txtStatus;
    // End of variables declaration//GEN-END:variables
}
