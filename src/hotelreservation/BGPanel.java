/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelreservation;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author titan
 */
public class BGPanel extends JPanel {
    private Image image;
    private byte backdrop_transparent = 0x7f;
    
    public BGPanel() {
        image = new ImageIcon(getClass().getResource("/gambar/bg.jpg")).getImage();
    }
    
    public BGPanel(String pict) {
        image  = new ImageIcon(getClass().getResource(pict)).getImage();
    }
    
    public void setAlpha(byte alpha) {
        this.backdrop_transparent = alpha;
        this.invalidate();
    }
    
    public byte getAlpha() {
        return this.backdrop_transparent;
    }
    
    public void setPicture(String pict) {
        image  = new ImageIcon(getClass().getResource(pict)).getImage();
        this.invalidate();
    }
    
    public Image getPicture() {
        return image;
    }
    
    @Override
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs);
        
        Color blackdrop = new Color(0, 0, 0, backdrop_transparent);

        Graphics2D gd = (Graphics2D) grphcs.create();
        gd.setColor(blackdrop);
        gd.drawImage(image, 0,0, getWidth(), getHeight(), null);
        gd.fillRect(0, 0, getWidth(), getHeight());
        gd.dispose();
    }
}
