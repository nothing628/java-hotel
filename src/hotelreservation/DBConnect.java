/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hotelreservation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author titan
 */
public class DBConnect {
    public static Connection conn;
    public static Connection getConnection() throws SQLException, InterruptedException {
        if (conn == null) {
            try {
                Class.forName("org.mariadb.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mariadb://localhost:3308/hotel?user=hotel&password=secret");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DBConnect.class.getName()).log(Level.WARNING, "Please add driver to your library", ex);
            }
        }
        
        return conn;
    }
}
