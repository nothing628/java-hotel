-- MariaDB dump 10.17  Distrib 10.5.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hotel
-- ------------------------------------------------------
-- Server version	10.5.4-MariaDB-1:10.5.4+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hotel`
--

/*!40000 DROP DATABASE IF EXISTS `hotel`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hotel` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `hotel`;

--
-- Table structure for table `tbl_harga`
--

DROP TABLE IF EXISTS `tbl_harga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_harga` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(30) NOT NULL,
  `harga` decimal(25,0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_harga`
--

LOCK TABLES `tbl_harga` WRITE;
/*!40000 ALTER TABLE `tbl_harga` DISABLE KEYS */;
INSERT INTO `tbl_harga` VALUES (2,'Anjay',30000),(3,'Delux',4000),(4,'Super',2000),(5,'test',12000),(7,'test',12300);
/*!40000 ALTER TABLE `tbl_harga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_kamar`
--

DROP TABLE IF EXISTS `tbl_kamar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kamar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_kamar` varchar(30) NOT NULL,
  `id_harga` bigint(20) unsigned DEFAULT NULL,
  `kamar_aktif` tinyint(1) NOT NULL DEFAULT 0,
  `status` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_kamar_FK` (`id_harga`),
  CONSTRAINT `tbl_kamar_FK` FOREIGN KEY (`id_harga`) REFERENCES `tbl_harga` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_kamar`
--

LOCK TABLES `tbl_kamar` WRITE;
/*!40000 ALTER TABLE `tbl_kamar` DISABLE KEYS */;
INSERT INTO `tbl_kamar` VALUES (1,'201',3,1,'kosong'),(2,'202',4,1,'kosong'),(4,'204',3,1,'kosong'),(6,'2222',2,0,'kosong');
/*!40000 ALTER TABLE `tbl_kamar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_karyawan`
--

DROP TABLE IF EXISTS `tbl_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_karyawan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` varchar(30) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_UN` (`username`),
  KEY `users_password_IDX` (`password`) USING BTREE,
  KEY `users_level_IDX` (`level`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_karyawan`
--

LOCK TABLES `tbl_karyawan` WRITE;
/*!40000 ALTER TABLE `tbl_karyawan` DISABLE KEYS */;
INSERT INTO `tbl_karyawan` VALUES (1,'herman','herman','karyawan',1),(2,'admin','admin','admin',1),(6,'anjay','aaaaa','Admin',1),(8,'test','test','Resepsionis',1);
/*!40000 ALTER TABLE `tbl_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pengunjung`
--

DROP TABLE IF EXISTS `tbl_pengunjung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pengunjung` (
  `id` varchar(30) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `is_blocked` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pengunjung`
--

LOCK TABLES `tbl_pengunjung` WRITE;
/*!40000 ALTER TABLE `tbl_pengunjung` DISABLE KEYS */;
INSERT INTO `tbl_pengunjung` VALUES ('12','test','Laki-Laki','1997-11-03',0),('test','test','Laki-Laki','1997-12-01',0);
/*!40000 ALTER TABLE `tbl_pengunjung` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_transaksi`
--

DROP TABLE IF EXISTS `tbl_transaksi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_transaksi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_kamar` bigint(20) unsigned NOT NULL,
  `id_pengunjung` varchar(30) NOT NULL,
  `tgl_checkin` date NOT NULL,
  `tgl_checkout` date DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `jml_bayar` decimal(25,0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `tbl_transaksi_FK` (`id_kamar`),
  KEY `tbl_transaksi_FK_1` (`id_pengunjung`),
  KEY `tbl_transaksi_status_IDX` (`status`) USING BTREE,
  CONSTRAINT `tbl_transaksi_FK` FOREIGN KEY (`id_kamar`) REFERENCES `tbl_kamar` (`id`),
  CONSTRAINT `tbl_transaksi_FK_1` FOREIGN KEY (`id_pengunjung`) REFERENCES `tbl_pengunjung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_transaksi`
--

LOCK TABLES `tbl_transaksi` WRITE;
/*!40000 ALTER TABLE `tbl_transaksi` DISABLE KEYS */;
INSERT INTO `tbl_transaksi` VALUES (1,1,'12','1998-12-12','1299-12-12','selesai',12),(6,1,'test','2020-08-12','2020-08-27','selesai',12),(7,1,'12','2020-08-10','2020-08-20','selesai',40000),(8,1,'12','2020-12-12','2020-12-14','selesai',8000);
/*!40000 ALTER TABLE `tbl_transaksi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-21  5:45:14
